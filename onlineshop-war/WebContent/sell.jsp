<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<h2>Artikel zum Verkauf anbieten</h2>
				<p> Alle Eingabefelder sind Plichtfelder *</p>
				<p>Maximal erlaubte Bilddateigr&ouml;sse: 1 MB</p>
				<p>Unterst&uuml;tzte Bilddateiformate: .png, .jpg oder .gif</p>
				<p> Hinweis zur Bildqualit&auml;t: Artikelbild wird auf 200x200 skaliert</p>
			</header>
		</article>
		<form action="sell" method="post" enctype="multipart/form-data">
			<fieldset>
				<div>
					<label for="title">* Titel:</label>
					<input type="text" name="title" placeholder="bspw. Holztisch" required="required"/>
				</div>
				<div>
					<label for="description">* Beschreibung:</label>
					<textarea name="description" rows="10" cols="50" placeholder="bspw. Sch&ouml;ner Holztisch aus Massivholz..." required="required"></textarea>
				</div>
				<label for="price">* Preis:</label>
				<div id="price">
					<input type="number" name="price" step="0.01" placeholder="bspw. 100" required="required"/>
				</div>
				<div id="currency">
					<select name="currency" required="required">
						<option value="" selected="selected" disabled hidden></option>
						<option value="CHF">CHF</option>
					</select>
				</div>
				<div>
					<label for="image">* Bild hochladen:</label>
					<input type="file" name="image" required="required" />
				</div>
				<div>
					<input type="submit" value="Zum Verkauf anbieten"/>
				</div>
				<div style="color: #008000; font-weight: bold;">
					${sellSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${sellNotSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${imageError}
				</div>
			</fieldset>
		</form>
		<ul class="links">
			<li><a href="buy.jsp">Zu kaufende Artikel durchsuchen</a></li>
		</ul>
	</section>
</main>
<%@ include file="footer.jspf" %>