<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${registrationNotSuccessful}
				</div>
				<h2>Jetzt registrieren beim Onlineshop</h2>
				<p>Alle Eingabefelder sind Pflichtfelder *</p>
			</header>
		</article>
		<form action="register" method="post">
			<fieldset>
				<div class="name">
					<label for="firstName">* Vorname:</label>
					<input type="text" name="firstName" placeholder="bspw. Hans" required="required"/>
				</div>
				<div class="name">
					<label for="lastName">* Nachname:</label>
					<input class="lastnamefield" type="text" name="lastName" placeholder="bspw. Muster" required="required"/>
				</div>
				<label for="emailAddress">* E-Mail-Adresse:</label>
				<input type="email" name="emailAddress" placeholder="bspw. hans.muster@example.com" required="required"/>
				<div class="password">
					<label for="password1">* Passwort:</label>
					<input type="password" name="password1" placeholder="Passwort" required="required" title="Muss mindestens eine Zahl und einen Gross- und Kleinbuchstaben sowie mindestens 8 Zeichen enthalten" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
				</div>
				<div class="password">
					<label for="password2">Passwort best&auml;tigen:</label>
					<input class="passwordfield" type="password" name="password2" placeholder="Passwort wiederholen" required="required" title="Muss mindestens eine Zahl und einen Gross- und Kleinbuchstaben sowie mindestens 8 Zeichen enthalten" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
				</div>
				<div>
					<input type="submit" value="Benutzerkonto erstellen"/>
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${passwordsDoNotMatchError}
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${emailAddressAlreadyExistsError}
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${invalidEmailAddress}
				</div>
			</fieldset>
			<ul class="links">
				<li>Besitzt du bereits ein Benutzerkonto? <a href="login.jsp">Jetzt anmelden!</a></li>
			</ul>
		</form>
	</section>
</main>
<%@ include file="footer.jspf" %>