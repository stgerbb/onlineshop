<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<div style="color: #008000; font-weight: bold; text-align: center;">
					${buyArticleSuccessful}
				</div>
				<div style="color: #008000; font-weight: bold; text-align: center;">
					${buyerEmailSuccessful}
				</div>
				<div style="color: #008000; font-weight: bold; text-align: center;">
					${sellerEmailSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${buyerEmailNotSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${sellerEmailNotSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${buyArticleNotSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${articleNotFoundError} 
				</div>
				<h2>Artikel suchen</h2>
				<p>Mit * markierte Eingabefelder sind Plichtfelder</p>
			</header>
		</article>
		<form action="search" method="post">
			<fieldset>
				<div>
					<label for="search">* Suchtext:</label>
					<input type="text" name="search" placeholder="bspw. Holztisch" required="required"/>
				</div>
				<div>
					<input type="submit" value="Suchen"/>
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${searchError}
				</div>
			</fieldset>
		</form>	
		<c:forEach items="${articles}" var="article">	
			<form action="buy" method="post">
				<fieldset>
					<c:set var="seller" value="Artikel von"/>
					<legend><c:out value="${seller} ${article.getSeller().getFirstName()} ${article.getSeller().getLastName()}"/></legend>
					<input type="hidden" name="articleId" value="<c:out value='${article.getId()}'/>"/>
					<h2><c:out value="${article.getTitle()}"/></h2>
					<p><c:out value="${article.getDescription()}"/></p>
					<p><c:out value="${article.getPrice()}"/></p>
					<p><c:out value="${article.getCurrency()}"/></p>
					<c:set var="image" value="${article.getImage()}"/>
					<%
						byte[] foto = (byte[]) pageContext.getAttribute("image");
						String image = new String(Base64.getEncoder().encode(foto));
						String articleImg = "data:image/png;base64," + image;
					%>
					<img id="search-image" src="<% out.println(articleImg); %>"/><br>			
					<input type="submit" value="Kaufen"/>
				</fieldset>
			</form>
		</c:forEach>
		<ul class="links">
			<li><a href="sell.jsp">Artikel zum Verkauf anbieten</a></li>
		</ul>
	</section>
</main>
<%@ include file="footer.jspf" %>