<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${emailNotSuccessful}  
				</div>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${notLoggedIn} 
				</div>
				<div style="color: #008000; font-weight: bold; text-align: center;">
					${verificationSuccessful} 
				</div>
				<h2>Jetzt anmelden am Onlineshop</h2>
				<p>E-Mail-Adresse und Passwort eingeben *</p>
			</header>
		</article>
		<form action="login" method="post">
			<fieldset>
				<div>
					<label for="emailAddress">* E-Mail-Adresse:</label>
					<input type="email" name="emailAddress" placeholder="bspw. hans.muster@example.com" required="required"/>
				</div>
				<div>
					<label for="password">* Passwort:</label>
					<input type="password" name="password" placeholder="Passwort" required="required"/>
				</div>
				<div>
					<input type="submit" value="Anmelden"/>
				</div>
				<div style="color: #008000; font-weight: bold;">
					${emailSendedSuccessful}
				</div>
				<div style="color: #008000; font-weight: bold;">
					${resetPasswordSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${userNotExistentError} 
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${loginNotSuccessful}
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${emailAddressNotExistent}
				</div>
			</fieldset>
		</form>
		<ul class="links">
			<li><a href="forgotpassword.jsp">Passwort vergessen?</a></li>
			<li>Kein Benutzerkonto? <a href="register.jsp">Benutzerkonto erstellen!</a></li>
		</ul>
	</section>
</main>
<%@ include file="footer.jspf" %>