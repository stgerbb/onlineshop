<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<h2>Passwort zur&uuml;cksetzen</h2>
				<p>Alle Eingabefelder sind Pflichtfelder *</p>
			</header>
		</article>
		<form action="reset-password" method="post">
			<fieldset>
				<div class="password">
					<label for="password1">* Neues Passwort:</label>
					<input type="password" name="password1" placeholder="Passwort" required="required" title="Muss mindestens eine Zahl und einen Gross- und Kleinbuchstaben sowie mindestens 8 Zeichen enthalten" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
				</div>
				<div class="password">
					<label for="password2">* Neues Passwort best&auml;tigen</label>
					<input type="password" name="password2" placeholder="Passwort" required="required" title="Muss mindestens eine Zahl und einen Gross- und Kleinbuchstaben sowie mindestens 8 Zeichen enthalten" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
				</div>
				<div>
					<input type="submit" value="Passwort &auml;ndern"/>
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${passwordsDoNotMatchError} 
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${resetPasswordNotSuccessful}
				</div>
			</fieldset>
		</form>
	</section>
</main>
<%@ include file="footer.jspf" %>