<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<h2>Zwei-Faktor-Authentifizierung</h2>
				<p>Ein E-Mail-Code wurde an die E-Mail-Adresse dieses Benutzerkontos versandt. Geben Sie den erhaltenen Code in das nachfolgende Eingabefeld ein.</p>
				<p>Alle Eingabefelder sind Plichtfelder *</p>
			</header>
		</article>
		<form action="email-code" method="post">
			<fieldset>
				<div>
					<label for="emailCode">E-Mail-Code:</label>
					<input type="number" name="emailCode" placeholder="bspw. 2753" required="required"/>
				</div>
				<input type="submit" value="Best&auml;tigen"/>
				<div style="color: #FF0000; font-weight: bold; text-align: center;">
					${wrongEmailCode}
				</div>
			</fieldset>
		</form>	
	</section>
</main>
<%@ include file="footer.jspf" %>