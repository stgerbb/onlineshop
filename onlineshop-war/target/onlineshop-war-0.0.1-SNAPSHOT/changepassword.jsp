<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<h2>Passwort &auml;ndern</h2>
				<p>Alle Eingabefelder sind Pflichtfelder *</p>
			</header>
		</article>
		<form action="change-password" method="post">
			<fieldset>
				<div>
					<label for="currentPassword">* Aktuelles Passwort:</label>
					<input type="password" name="currentPassword" placeholder="Passwort" required="required"/>
				</div>
				<div class="password">
					<label for="newPassword1">* Neues Passwort:</label>
					<input type="password" name="newPassword1" placeholder="Passwort" required="required" title="Muss mindestens eine Zahl und einen Gross- und Kleinbuchstaben sowie mindestens 8 Zeichen enthalten" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
				</div>
				<div class="password">
					<label for="newPassword2">* Neues Passwort wiederholen:</label>
					<input type="password" name="newPassword2" placeholder="Passwort" required="required" title="Muss mindestens eine Zahl und einen Gross- und Kleinbuchstaben sowie mindestens 8 Zeichen enthalten" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
				</div>
				<div>
					<input type="submit" value="Passwort &auml;ndern"/>
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${passwordsDoNotMatchError}
				</div>
			</fieldset>
		</form>
	</section>
</main>
<%@ include file="footer.jspf" %>