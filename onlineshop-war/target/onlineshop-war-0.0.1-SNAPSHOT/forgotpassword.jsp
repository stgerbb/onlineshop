<%@ include file="header.jspf" %>
<main>
	<section>
		<article>
			<header>
				<h2>Passwort zur&uuml;cksetzen</h2>
				<p>Ein Link zum Zur&uuml;cksetzen Ihres Passworts wird an die folgende im Onlineshop registrierte E-Mail-Adresse verschickt.</p>
				<p>Alle Eingabefelder sind Pflichtfelder *</p>
			</header>
		</article>
		<form action="forgot-password" method="post">
			<fieldset>
				<div>
					<label for="emailAddress">* E-Mail-Adresse:</label>
					<input type="email" name="emailAddress" placeholder="bspw. hans.muster@example.com" required="required"/>
				</div>
				<div>
					<input type="submit" value="Senden"/>
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${emailNotExistentError}
				</div>
				<div style="color: #FF0000; font-weight: bold;">
					${emailNotSendedError}
				</div>
			</fieldset>
		</form>
	</section>
</main>
<%@ include file="footer.jspf" %>