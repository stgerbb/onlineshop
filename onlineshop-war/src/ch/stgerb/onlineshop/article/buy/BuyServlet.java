package ch.stgerb.onlineshop.article.buy;

import java.io.IOException;
import java.util.Base64;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.ArticleEngine;
import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.Article;
import ch.stgerb.onlineshop.db.entity.User;
import ch.stgerb.onlineshop.mail.MailController;

/**
 * @author Stefan Gerber
 * @date 2018-11-21
 */
@WebServlet("/buy")
public class BuyServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ArticleEngine ae;
	
	@Inject
	private MailController mail;
	
	@Inject
	private UserEngine ue;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		long articleId = Long.parseLong(req.getParameter("articleId"));
		
		User buyer = (User) req.getSession().getAttribute("user");
		
		Article article = this.ae.getArticle(articleId);
		if (article != null) {
			article.setBuyer(buyer);
			
			if (this.ae.updateArticle(article)) {
				req.setAttribute("buyArticleSuccessful", "Artikel wurde erfolgreich erworben.");
				
				String byuerEmailAddress = buyer.getEmailAddress();
				String buyerFirstName = buyer.getFirstName();
				String buyerLastName = buyer.getLastName();
				
				byte[] foto = article.getImage();
				String image = new String(Base64.getEncoder().encode(foto));
				String src = "data:image/png;base64," + image;
				
				long sellerId = article.getSeller().getId();
				User seller = this.ue.getUser(sellerId);
				String sellerEmailAddress = seller.getEmailAddress();
				String sellerFirstName = seller.getFirstName();
				String sellerLastName = seller.getLastName();
				
				String articleTitle = article.getTitle();
				String articleDescription = article.getDescription();
				String articleCurrency = article.getCurrency();
				double articlePrice = article.getPrice();
				
				if (this.mail.isArticleBoughtEmailSended(byuerEmailAddress, sellerEmailAddress, sellerFirstName, sellerLastName, articleTitle, articleDescription, articleCurrency, articlePrice, src)) {
					req.setAttribute("buyerEmailSuccessful", "Eine Kaufbestätigung des gerade erworbenen Artikels wurde an " + byuerEmailAddress + " versandt.");
				} else {
					req.setAttribute("buyerEmailNotSuccessful", "Fehler beim Versand der Kaufbestätigung des gerade erworbenen Artikels an " +  byuerEmailAddress);
				}
				
				if (this.mail.isArticleSelledEmailSended(sellerEmailAddress, byuerEmailAddress, buyerFirstName, buyerLastName, articleTitle, articleDescription, articleCurrency, articlePrice, src)) {
					req.setAttribute("sellerEmailSuccessful", "Der Verkäufer wurde via E-Mail über den Erwerb seines Artikels informiert.");
					req.getRequestDispatcher("buy.jsp").forward(req, resp);
				} else {
					req.setAttribute("sellerEmailNotSuccessful", "Fehler beim Informieren des Verkäufers über den erworbenen Artikels.");
					req.getRequestDispatcher("buy.jsp").forward(req, resp);
				}
			} else {
				req.setAttribute("buyArticleNotSuccessful", "Fehler beim erwerben des Artikels.");
				req.getRequestDispatcher("buy.jsp").forward(req, resp);
			}
		} else {
			req.setAttribute("articleNotFoundError", "Der gewünschte Artikel konnte in der Datenbank nicht gefunden werden.");
			req.getRequestDispatcher("buy.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("buy.jsp").forward(req, resp);
	}
}