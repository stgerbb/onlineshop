package ch.stgerb.onlineshop.article.buy;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.ArticleEngine;
import ch.stgerb.onlineshop.db.entity.Article;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/search")
public class SearchServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private ArticleEngine ae;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		String search = req.getParameter("search");
		
		ArrayList<Article> articles = this.ae.getBuyableArticles(search);
		
		if (articles != null) {
			req.setAttribute("articles", articles);
			req.getRequestDispatcher("buy.jsp").forward(req, resp);
		} else {
			req.setAttribute("searchError", "Es wurden keine Einträge für den Suchbegriff " + "\"" + search  + "\" gefunden.");
			req.getRequestDispatcher("buy.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("buy.jsp").forward(req, resp);
	}
}