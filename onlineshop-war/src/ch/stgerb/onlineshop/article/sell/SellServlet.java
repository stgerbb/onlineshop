package ch.stgerb.onlineshop.article.sell;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import ch.stgerb.onlineshop.db.engine.ArticleEngine;
import ch.stgerb.onlineshop.db.engine.ImageController;
import ch.stgerb.onlineshop.db.entity.Article;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/sell")
@MultipartConfig(maxFileSize=1024*1024)
public class SellServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ImageController ic;
	
	@Inject
	private ArticleEngine ae;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		String title = req.getParameter("title").trim();
		String description = req.getParameter("description").trim();
		double price = Double.parseDouble(req.getParameter("price"));
		String currency = req.getParameter("currency");
		Part foto = req.getPart("image");
		
		byte[] image = this.ic.convertToByteArray(foto);
		if (image != null) {
			Article article = new Article();
			article.setTitle(title);
			article.setDescription(description);
			article.setPrice(price);
			article.setCurrency(currency);
			article.setImage(image);
			
			User user = (User) req.getSession().getAttribute("user");
			article.setSeller(user);
			
			if (this.ae.saveArticle(article)) {
				req.setAttribute("sellSuccessful", "Artikel wurde erfolgreich gespeichert.");
				req.getRequestDispatcher("sell.jsp").forward(req, resp);
			} else {
				req.setAttribute("sellNotSuccessful", "Fehler bei der Speicherung des Artikels.");
				req.getRequestDispatcher("sell.jsp").forward(req, resp);
			}
		} else {
			req.setAttribute("imageError", "Fehler bei der Verarbeitung des Bildes.");
			req.getRequestDispatcher("sell.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("sell.jsp").forward(req, resp);
	}
}