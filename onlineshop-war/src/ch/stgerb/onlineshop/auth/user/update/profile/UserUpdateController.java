package ch.stgerb.onlineshop.auth.user.update.profile;


/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
public class UserUpdateController {
	
	public int[] assembleBirthdate(int day, int month, int year) {
		int[] birthdate = new int[3];
		
		if (day == 0 || month == 0 || year == 0) {
			birthdate[0] = 0;
			birthdate[1] = 0;
			birthdate[2] = 0;
		} else {
			birthdate[0] = day;
			birthdate[1] = month;
			birthdate[2] = year;
		}
		
		return birthdate;
	}
}
