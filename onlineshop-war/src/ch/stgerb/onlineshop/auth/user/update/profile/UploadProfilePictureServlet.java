package ch.stgerb.onlineshop.auth.user.update.profile;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import ch.stgerb.onlineshop.db.engine.ImageController;
import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-17
 */
@WebServlet("/upload-profile-picture")
@MultipartConfig(maxFileSize=1024*1024)
public class UploadProfilePictureServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ImageController ic;
	
	@Inject
	private UserEngine ue;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		Part foto = req.getPart("image");
		byte[] image = this.ic.convertToByteArray(foto);
		if (image != null) {
			User user = (User) req.getSession().getAttribute("user");
			user.setImage(image);
			
			if (this.ue.updateUser(user)) {
				req.setAttribute("profilePictureSuccessful", "Profilbild wurde erfolgreich gespeichert.");
				req.getRequestDispatcher("account.jsp").forward(req, resp);
			} else {
				req.setAttribute("profilePictureNotSuccessful", "Fehler beim Speichern des Profilbildes.");
				req.getRequestDispatcher("account.jsp").forward(req, resp);
			}
		} else {
			req.setAttribute("profilePictureError", "Fehler bei der Verarbeitung des Profilbildes.");
			req.getRequestDispatcher("account.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("account.jsp").forward(req, resp);
	}
}