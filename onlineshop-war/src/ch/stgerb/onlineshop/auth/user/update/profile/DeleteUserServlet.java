package ch.stgerb.onlineshop.auth.user.update.profile;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/delete-user")
public class DeleteUserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserEngine ue;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		User user = (User) req.getSession().getAttribute("user");
		if (this.ue.deleteUser(user)) {
			req.setAttribute("userDeletionSuccessful", "Benutzerkonto wurde erfolgreich gelöscht.");
			req.setAttribute("logoutSuccessful", "Abmeldung erfolgreich.");
			req.getRequestDispatcher("index.jsp").forward(req, resp);
		} else {
			req.setAttribute("userDeletionNotSuccessful", "Fehler beim Löschen des Benutzerkontos. Keine Änderungen wurden vorgenommen.");
			req.getRequestDispatcher("account.jsp").forward(req, resp);
		}
	}
}