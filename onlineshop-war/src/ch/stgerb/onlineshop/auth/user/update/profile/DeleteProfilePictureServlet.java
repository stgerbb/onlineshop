package ch.stgerb.onlineshop.auth.user.update.profile;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-17
 */
@WebServlet("/delete-profile-picture")
public class DeleteProfilePictureServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserEngine ue;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		User user = (User) req.getSession().getAttribute("user");
		
		user.setImage(null);
		
		if (this.ue.updateUser(user)) {
			req.setAttribute("deleteProfilePictureSuccessful", "Profilbild wurde erfolgreich entfernt.");
			req.getRequestDispatcher("account.jsp").forward(req, resp);
		} else {
			req.setAttribute("deleteProfilePictureNotSuccessful", "Fehler beim Löschen des Profilbilds.");
			req.getRequestDispatcher("account.jsp").forward(req, resp);
		}
	}
}