package ch.stgerb.onlineshop.auth.user.update.profile;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/update-user-account")
public class UserUpdateServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserEngine ue;
	
	@Inject
	private UserUpdateController uuc;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		String firstName = req.getParameter("firstName").trim();
		String lastName = req.getParameter("lastName").trim();
		String salutation = req.getParameter("salutation");
		int day = Integer.parseInt(req.getParameter("day"));
		int month = Integer.parseInt(req.getParameter("month"));
		int year = Integer.parseInt(req.getParameter("year"));
		String street = req.getParameter("street").trim();
		String streetNum = req.getParameter("streetNum").trim().replace(" ", "");
		String zip = req.getParameter("zip").trim().replace(" ", "");
		String city = req.getParameter("city").trim();
		String country = req.getParameter("country");
		String phoneNum = req.getParameter("phoneNum").trim().replace(" ", "");
		
		User user = (User) req.getSession().getAttribute("user");
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setSalutation(salutation);
		
		int[] birthdate = this.uuc.assembleBirthdate(day, month, year);
		user.setBirthdate(birthdate[0], birthdate[1], birthdate[2]);
		
		user.setStreet(street);
		user.setStreetNum(streetNum);
		user.setZip(zip);
		user.setCity(city);
		user.setCountry(country);
		user.setPhoneNum(phoneNum);
		
		if (this.ue.updateUser(user)) {
			req.setAttribute("userUpdateSuccessful", "Änderungen wurden erfolgreich überommen.");
			req.getRequestDispatcher("account.jsp").forward(req, resp);
		} else {
			req.setAttribute("userUpdateNotSuccessful", "Fehler beim Übernehmen der Änderungen. Es wurden keine Veränderungen vorgenommen.");
			req.getRequestDispatcher("account.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("account.jsp").forward(req, resp);
	}
}