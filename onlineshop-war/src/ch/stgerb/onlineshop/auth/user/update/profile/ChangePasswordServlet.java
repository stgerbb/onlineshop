package ch.stgerb.onlineshop.auth.user.update.profile;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.crypto.tink.KeysetHandle;

import ch.stgerb.onlineshop.auth.crypto.Crypto;
import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/change-password")
public class ChangePasswordServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Crypto crypto;
	
	@Inject
	private UserEngine ue;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");

		String currentPassword = req.getParameter("currentPassword");
		String newPassword1 = req.getParameter("newPassword1");
		String newPassword2 = req.getParameter("newPassword2");
		
		User user = (User) req.getSession().getAttribute("user");
		byte[] password = user.getPassword();
		
		String contextPath = getServletContext().getRealPath("/");
		KeysetHandle keyset = this.crypto.getKeysetHandle(contextPath);
		String decrypted = this.crypto.decrypt(password, keyset);
		
		if (!decrypted.equals(currentPassword)) {
			req.setAttribute("passwordsDoNotMatchError", "Das aktuelle Passwort stimmt nicht überein.");
			req.getRequestDispatcher("changepassword.jsp").forward(req, resp);
		} else if (!newPassword1.equals(newPassword2)) {
			req.setAttribute("passwordsDoNotMatchError", "Die neuen Passwörter stimmen nicht überein.");
			req.getRequestDispatcher("changepassword.jsp").forward(req, resp);
		} else {
			byte[] encrypted = this.crypto.encryt(newPassword1, keyset);

			user.setPassword(encrypted);
			
			if (this.ue.updateUser(user)) {
				req.setAttribute("passwordChangeSuccessful", "Passwort wurde erfolgreich aktualisiert.");
				req.getRequestDispatcher("account.jsp").forward(req, resp);
			} else {
				req.setAttribute("passwordChangeError", "Fehler beim Aktualisieren des Passworts. Es wurden keine Veränderungen vorgenommen.");
				req.getRequestDispatcher("account.jsp").forward(req, resp);
			}
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("account.jsp").forward(req, resp);
	}
}