package ch.stgerb.onlineshop.auth.login;

import javax.inject.Inject;

import com.google.crypto.tink.KeysetHandle;

import ch.stgerb.onlineshop.auth.crypto.Crypto;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
public class LoginController {
	
	@Inject
	private Crypto crypto;
	
	public boolean validatePassword(User user, String password, String directroy) {
		byte[] encrypted = user.getPassword();
		KeysetHandle keysetHandle = this.crypto.getKeysetHandle(directroy);
		String decrypted = this.crypto.decrypt(encrypted, keysetHandle);
		if (decrypted.equals(password)) {
			return true;
		} else {
			return false;
		}
	}
}