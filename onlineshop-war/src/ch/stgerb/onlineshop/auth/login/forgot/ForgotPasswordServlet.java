package ch.stgerb.onlineshop.auth.login.forgot;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.mail.MailController;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/forgot-password")
public class ForgotPasswordServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserEngine ue;
	
	@Inject
	private MailController mail;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		String emailAddress = req.getParameter("emailAddress").trim().replace(" ", "");
		
		if (this.ue.isEmailAddressExistent(emailAddress)) {
			
			if (this.mail.isForgotPasswordEmailSended(emailAddress)) {
				req.getSession().setAttribute("emailAddress", emailAddress);
				req.setAttribute("emailSendedSuccessful", "E-Mail wurde erfolgreich an " + emailAddress + " versandt.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			} else {
				req.setAttribute("emailNotSendedError", "Fehler beim versenden des E-Mails.");
				req.getRequestDispatcher("forgotpassword.jsp").forward(req, resp);
			}
		} else {
			req.setAttribute("emailNotExistentError", "Es wurde kein registriertes Benutzerkonto zur eingegebenen E-Mail-Adresse gefunden.");
			req.getRequestDispatcher("forgotpassword.jsp").forward(req, resp);
		}
	}
}