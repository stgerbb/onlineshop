package ch.stgerb.onlineshop.auth.login.forgot;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Stefan Gerber
 * @date 2018-11-21
 */
public class ResetPasswordFilter implements Filter{

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("emailAddress") == null) {
			request.getRequestDispatcher("forgotpassword.jsp").forward(request, response);
		} else {
			chain.doFilter(request, response);
		}
	}
}