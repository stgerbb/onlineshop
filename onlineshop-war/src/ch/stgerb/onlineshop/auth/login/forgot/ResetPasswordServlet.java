package ch.stgerb.onlineshop.auth.login.forgot;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.crypto.tink.KeysetHandle;

import ch.stgerb.onlineshop.auth.crypto.Crypto;
import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/reset-password")
public class ResetPasswordServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserEngine ue;
	
	@Inject
	private Crypto crypto;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		String password1 = req.getParameter("password1");
		String password2 = req.getParameter("password2");

		if (password1.equals(password2)) {
			String contextPath = getServletContext().getRealPath("/");
			KeysetHandle keysetHandle = this.crypto.getKeysetHandle(contextPath);
			byte[] encrypted = this.crypto.encryt(password1, keysetHandle);
			
			String emailAddress = (String) req.getSession().getAttribute("emailAddress");
			User user = this.ue.getUserByEmailAddress(emailAddress);
			
			if (user != null) {
				user.setPassword(encrypted);
				
				if (this.ue.updateUser(user)) {
					req.setAttribute("resetPasswordSuccessful", "Das Passwort für " + emailAddress + " wurde erfolgreich zurückgesetzt.");
					req.getRequestDispatcher("login.jsp").forward(req, resp);
				} else {
					req.setAttribute("resetPasswordNotSuccessful", "Fehler beim Zurücksetzen des Passworts.");
					req.getRequestDispatcher("resetpassword.jsp").forward(req, resp);
				}
			} else {
				req.setAttribute("emailAddressNotExistent", "Fataler Fehler. Vorgang wurde abgebrochen.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} else {
			req.setAttribute("passwordsDoNotMatchError", "Passwörter stimmen nicht überein.");
			req.getRequestDispatcher("resetpassword.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("forgotpassword.jsp").forward(req, resp);
	}
}