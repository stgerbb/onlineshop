package ch.stgerb.onlineshop.auth.login;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.entity.User;
import ch.stgerb.onlineshop.mail.MailController;

@WebServlet("/resend-email-code")
public class ResendEmailCodeServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private MailController mail;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		User user = (User) req.getSession().getAttribute("user");
		String emailAddress = user.getEmailAddress();
		int generatedCode = (int) req.getSession().getAttribute("generatedCode");
		
		if (this.mail.isEmailCodeSended(emailAddress, generatedCode)) {
			req.setAttribute("emailCodeResendedSuccessful", "E-Mail-Code wurde erneut an " + emailAddress + " versandt.");
			req.getRequestDispatcher("2fa.jsp").forward(req, resp);
		} else {
			req.setAttribute("emailNotSuccessful", "Fehler beim erneuten senden des E-Mail-Codes an " + emailAddress);
			req.getRequestDispatcher("2fa.jsp").forward(req, resp);
		}
	}	
}