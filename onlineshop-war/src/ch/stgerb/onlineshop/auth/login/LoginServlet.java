package ch.stgerb.onlineshop.auth.login;

import java.io.IOException;
import java.util.Random;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;
import ch.stgerb.onlineshop.mail.MailController;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private LoginController lc;
	
	@Inject
	private UserEngine ue;
	
	@Inject
	private MailController mail;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		String emailAddress = req.getParameter("emailAddress").trim().replace(" ", "");
		String password = req.getParameter("password");
		
		User user = this.ue.getUserByEmailAddress(emailAddress);
		
		if (user != null) {
			String contextPath = getServletContext().getRealPath("/");
			
			if (lc.validatePassword(user, password, contextPath)) {
				Random rand = new Random();
				int generatedCode = rand.nextInt(99999999 - 12345678 + 1) + 12345678;
				
				if(this.mail.isEmailCodeSended(emailAddress, generatedCode)) {
					req.getSession().setAttribute("generatedCode", generatedCode);
					req.getSession().setAttribute("login", user);
					req.getRequestDispatcher("2fa.jsp").forward(req, resp);
				} else {
					req.setAttribute("emailNotSuccessful", "Fehler beim Versenden des E-Mail-Codes an " + emailAddress);
					req.getRequestDispatcher("login.jsp").forward(req, resp);
				}
			} else {
				req.setAttribute("loginNotSuccessful", "Falsche E-Mail-Adresse oder Passwort.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} else {
			req.setAttribute("loginNotSuccessful", "Falsche E-Mail-Adresse oder Passwort.");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}
}