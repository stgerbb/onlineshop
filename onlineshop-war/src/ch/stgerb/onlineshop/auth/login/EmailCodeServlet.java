package ch.stgerb.onlineshop.auth.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-22
 */
@WebServlet("/email-code")
public class EmailCodeServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		int generatedCode = (int) req.getSession().getAttribute("generatedCode");
		User user = (User) req.getSession().getAttribute("login");
		int emailCode = Integer.parseInt(req.getParameter("emailCode").trim());
		
		if (generatedCode == emailCode) {
			req.getSession().invalidate();
			req.getSession().setAttribute("user", user);
			req.setAttribute("loginSuccessful", "Anmeldung erfolgreich.");
			req.getRequestDispatcher("index.jsp").forward(req, resp);
		} else {
			req.setAttribute("wrongEmailCode", "Ungültiger E-Mail-Code.");
			req.getRequestDispatcher("2fa.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}
}