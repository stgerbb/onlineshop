package ch.stgerb.onlineshop.auth.registration;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-22
 */
@WebServlet("/verify-registration")
public class VerifyRegistrationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserEngine ue;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User user = (User) req.getSession().getAttribute("registration");
		
		if (this.ue.saveUser(user)) {
			req.getSession().invalidate();
			req.setAttribute("verificationSuccessful", "Bestätigung erfolgreich. Benutzerkonto wurde erfolgreich erstellt. Du kannst Dich nun mit diesem anmelden.");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		} else {
			req.getSession().invalidate();
			req.setAttribute("registrationNotSuccessful", "Fehler beim Erstellen des Benutzerkontos.");
			req.getRequestDispatcher("register.jsp").forward(req, resp);
		}
	}
}