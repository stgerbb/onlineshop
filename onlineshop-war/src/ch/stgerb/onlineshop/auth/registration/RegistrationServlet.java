package ch.stgerb.onlineshop.auth.registration;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.crypto.tink.KeysetHandle;

import ch.stgerb.onlineshop.auth.crypto.Crypto;
import ch.stgerb.onlineshop.db.engine.UserEngine;
import ch.stgerb.onlineshop.db.entity.User;
import ch.stgerb.onlineshop.mail.MailController;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@WebServlet("/register")
public class RegistrationServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserEngine ue;
	
	@Inject
	private Crypto crypto;
	
	@Inject
	private MailController mail;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		
		String firstName = req.getParameter("firstName").trim();
		String lastName = req.getParameter("lastName").trim();
		String emailAddress = req.getParameter("emailAddress").trim().replace(" ", "");
		String password1 = req.getParameter("password1");
		String password2 = req.getParameter("password2");
		
		if (!password1.equals(password2)) {
			req.setAttribute("passwordsDoNotMatchError", "Passwörter stimmen nicht überein.");
			req.getRequestDispatcher("register.jsp").forward(req, resp);
		} else if (this.ue.isEmailAddressExistent(emailAddress)) {
			req.setAttribute("emailAddressAlreadyExistsError", "Die E-Mail-Adresse " + emailAddress + " ist im Onlineshop bereits registriert und kann nicht erneut verwendet werden.");
			req.getRequestDispatcher("register.jsp").forward(req, resp);
		} else {
			String contextPath = getServletContext().getRealPath("/");
			KeysetHandle keyset = this.crypto.getKeysetHandle(contextPath);
			byte[] password = this.crypto.encryt(password1, keyset);
			
			User user = new User();
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmailAddress(emailAddress);
			user.setPassword(password);
			user.setBirthdate(0, 0, 0);
			
			if (mail.isRegistrationEmailSended(emailAddress)) {
				req.getSession().setAttribute("registration", user);
				req.setAttribute("registrationSuccessful", "Eine E-Mail wurde an " + emailAddress + " versandt. Folge den Anweisungen in der E-Mail für die definitive Erstellung deines Benutzerkontos.");
				req.getRequestDispatcher("index.jsp").forward(req, resp);
			} else {
				req.setAttribute("invalidEmailAddress", "Fehler beim Versenden der Registrationsbestätigung.");
				req.getRequestDispatcher("register.jsp").forward(req, resp);
			}
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("register.jsp").forward(req, resp);
	}
}