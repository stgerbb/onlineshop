package ch.stgerb.onlineshop.auth.crypto.test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.crypto.tink.CleartextKeysetHandle;
import com.google.crypto.tink.JsonKeysetWriter;
import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.aead.AeadConfig;
import com.google.crypto.tink.aead.AeadKeyTemplates;

/**
 * @author Stefan Gerber
 * @date 2018-11-22
 */
@WebServlet("/keyset-generator-test")
public class KeysetGeneratorTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=UTF-8");
		PrintWriter printWriter = resp.getWriter();
		printWriter.println("<!DOCTYPE html>");
		printWriter.println("<html>");
		printWriter.println("<head>");
		printWriter.println("</head>");
		printWriter.println("<body>");
		printWriter.println("<h1>Keyset Generator starting!</h1>");
		String contextPath = getServletContext().getRealPath("/");
		File file = new File(contextPath + "\\..\\..\\..\\keyset.cfg");
		file.createNewFile();
		KeysetHandle keyset = null;
		try {
			AeadConfig.register();
			keyset = KeysetHandle.generateNew(AeadKeyTemplates.AES128_GCM);
			CleartextKeysetHandle.write(keyset, JsonKeysetWriter.withFile(file));
		} catch (Exception ex1) {
			printWriter.println("<p>" + ex1.getMessage() + "</p>");
		}
		printWriter.println("<h2>keyset.cfg created successfully under: " + contextPath);
		printWriter.println("<h1>Keyset Generator finished!</h1>");
		printWriter.println("</body>");
		printWriter.println("</html>");
	}
}