package ch.stgerb.onlineshop.auth.crypto;

import java.io.File;
import java.io.IOException;

import com.google.crypto.tink.Aead;
import com.google.crypto.tink.CleartextKeysetHandle;
import com.google.crypto.tink.JsonKeysetReader;
import com.google.crypto.tink.JsonKeysetWriter;
import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.aead.AeadConfig;
import com.google.crypto.tink.aead.AeadFactory;
import com.google.crypto.tink.aead.AeadKeyTemplates;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
public class Crypto {
	
	private static final String AAD = "javaeeffhs2018";
	
	public Crypto() {
		try {
			AeadConfig.register();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private File getKeysetFile(String directroy) {
		File file = null;
		try {
			file = new File(directroy + "\\..\\..\\..\\keyset.cfg");
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return file;
	}
	
	private KeysetHandle createNewKeysetHandle(String directroy) {
		File file = this.getKeysetFile(directroy);
		KeysetHandle keysetHandle = null;
		try {
			keysetHandle = KeysetHandle.generateNew(AeadKeyTemplates.AES128_GCM);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			CleartextKeysetHandle.write(keysetHandle, JsonKeysetWriter.withFile(file));
		} catch (IOException e) {
			System.out.println("IOException: " +  e.getMessage());
		}
		return keysetHandle;
	}
	
	public KeysetHandle getKeysetHandle(String directory) {
		File file = this.getKeysetFile(directory);
		if (file.exists()) {
			try {
				return CleartextKeysetHandle.read(JsonKeysetReader.withFile(file));
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return this.createNewKeysetHandle(directory);
	}
	
	public byte[] encryt(String string, KeysetHandle keyset) {
		byte[] encrypted = null;
		try {
			Aead aead = AeadFactory.getPrimitive(keyset);
			encrypted = aead.encrypt(string.getBytes(), Crypto.AAD.getBytes());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return encrypted;
	}

	public String decrypt(byte[] password, KeysetHandle keyset) {
		byte[] decrypted = null;
		try {
			Aead aead = AeadFactory.getPrimitive(keyset);
			decrypted = aead.decrypt(password, Crypto.AAD.getBytes());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		String plaintext = new String(decrypted);
		return plaintext;
	}
}