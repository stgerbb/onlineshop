package ch.stgerb.onlineshop.mail;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
public class MailController {
	
	@Resource(name="mail/gmail")
	private Session session;
	
	private boolean isSended(String emailAddress, String subject, String content) {
		Message message = new MimeMessage(this.session);
		try {
			message.setRecipient(RecipientType.TO, new InternetAddress(emailAddress));
			message.setSubject(subject);
			message.setContent(content, "text/html; charset=UTF-8");
			Transport.send(message);
		} catch(Exception ex2) {
			return false;
		}
		return true;
	}
	
	public boolean isEmailCodeSended(String emailAddress, int generatedCode) {
		String subject = "Onlineshop - E-Mail-Code";
		
		StringBuilder message = new StringBuilder();
		message.append("<h2>E-Mail-Code</h2>");
		message.append("<p>E-Mail-Code: " + generatedCode);
		
		if (this.isSended(emailAddress, subject, message.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isRegistrationEmailSended(String emailAddress) {
		String subject = "Onlineshop - Registration bestätigen";
		
		StringBuilder message = new StringBuilder();
		message.append("<h2>E-Mail-Adresse bestätigen</h2>");
		message.append("<p>Für die definitive Erstellung ihres Benutzerkontos im Onlineshop bitten wir Sie, ihre E-Mail-Adresse zu bestätigen:</p>");
		message.append("<a href='http://localhost:8080/onlineshop-war/verify-registration'>E-Mail-Adresse bestätigen</a>");
		message.append("<p>Falls der obige Link nicht funktionieren sollte, verwenden Sie diesen alternativ Link (in die Adressleiste ihres gewünschten Browsers kopieren):</p>");
		message.append("<p>http://localhost:8080/onlineshop-war/verify-registration</p>");
		
		if (this.isSended(emailAddress, subject, message.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isForgotPasswordEmailSended(String emailAddress) {
		String subject = "Onlineshop - Anfrage zum Zurücksetzen des Passworts";
		
		StringBuilder message = new StringBuilder();
		message.append("<h2>Anfrage zur Zurücksetzung des Passworts ihres Benutzerkontos im Onlineshop</h2>");
		message.append("<p>Für ihr Benutzerkonto im Onlineshop wurde eine Anfrage zur Zurücksetzung des Passworts beantragt.</p>");
		message.append("<p>Über den nachfolgenden Link können Sie ein neues Passwort für Ihr Benutzerkonto im Onlineshop beantragen:</p>");
		message.append("<a href='http://localhost:8080/onlineshop-war/resetpassword.jsp'>Passwort zurücksetzen</a>");
		message.append("<p>Falls der obige Link nicht funktionieren sollte, verwenden Sie diesen alternativ Link (in die Adressleiste ihres gewünschten Browsers kopieren):</p>");
		message.append("<p>http://localhost:8080/onlineshop-war/resetpassword.jsp</p>");
		
		if (this.isSended(emailAddress, subject, message.toString())) {
			return true;
		} else 
			return false;
	}
	
	public boolean isArticleSelledEmailSended(String sellerEmailAddress, String buyerEmailAddress,  String buyerFirstName, String buyerLastName, String articleTitle, String articleDescription, String articleCurrency, double articlePrice, String articleFoto) {
		String subject = "Onlineshop - Erwerbsinformation zum Artikel " + articleTitle;
		
		StringBuilder message = new StringBuilder();
		message.append("<h2>Erwerbsinformation zum Artikel " + articleTitle + "</h2>");
		message.append("<p>Ihr Artikel wurde im Onlineshop erworben:</p>");
		message.append("<p>Artikel: " + articleTitle + "</p>");
		message.append("<p>Beschreibung: " + articleDescription + "</p>");
		message.append("<p>Preis: " + articleCurrency + " " + articlePrice + "</p>");
		message.append("<img src=" + articleFoto + "/>");
		message.append("<p>Sie können entweder auf eine Antwort des Käufers warten oder mit den nachfolgenden Daten diesen selber kontaktieren, um das weitere Vorgehen wie Zahlung sowie Lieferung/Abholung zu vereinbaren:</p>");
		message.append("<p>Name: " + buyerFirstName + " " + buyerLastName + "</p>");
		message.append("<p>E-Mail-Adresse: " + buyerEmailAddress + "</p>");
		
		if (this.isSended(sellerEmailAddress, subject, message.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isArticleBoughtEmailSended(String buyerEmailAddress, String sellerEmailAddress, String sellerFirstName, String sellerLastName, String articleTitle, String articleDescription, String articleCurrency, double articlePrice, String articleFoto) {
		String subject = "Onlineshop - Kaufbestätigung zum Artikel " + articleTitle;
		
		StringBuilder message = new StringBuilder();
		message.append("<h2>Kaufbestätigung zum Artikel " + articleTitle + "</h2>");
		message.append("<p>Sie haben folgenden Artikel im Onlineshop erworben:</p>");
		message.append("<p>Artikel: " + articleTitle + "</p>");
		message.append("<p>Beschreibung: " + articleDescription + "</p>");
		message.append("<p>Preis: " + articleCurrency + " " + articlePrice + "</p>");
		message.append("<img src=" + articleFoto + "/>");
		message.append("<p>Mit den nachfolgenden Daten können Sie sich mit dem Verkäufer ihres erworbenen Artikels in Kontakt setzen, um das weitere Vorgehen wie Zahlung sowie Lieferung/Abholung zu vereinbaren:</p>");
		message.append("<p>Name: " + sellerFirstName + " " + sellerLastName + "</p>");
		message.append("<p>E-Mail-Adresse: " + sellerEmailAddress + "</p>");
		
		if (this.isSended(buyerEmailAddress, subject, message.toString())) {
			return true;
		} else {
			return false;
		}
	}
}