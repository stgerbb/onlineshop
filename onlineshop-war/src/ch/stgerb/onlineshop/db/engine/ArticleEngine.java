package ch.stgerb.onlineshop.db.engine;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import ch.stgerb.onlineshop.db.entity.Article;
import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
public class ArticleEngine {
	
	@PersistenceContext
	private EntityManager em;
	
	@Resource
	private UserTransaction ut;
	
	public boolean saveArticle(Article article) {
		try {
			this.ut.begin();
			this.em.persist(article);
			this.ut.commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean updateArticle(Article article) {
		try {
			this.ut.begin();
			this.em.merge(article);
			this.ut.commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean deleteArticle(Article article) {
		try {
			this.ut.begin();
			Article currentArticle = this.em.merge(article);
			this.em.remove(currentArticle);
			this.ut.commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public Article getArticle(long id) {
		Article article = this.em.find(Article.class, id);
		
		if (article != null) {
			return article;
		} else {
			return null;
		}
	}
	
	public ArrayList<Article> getBuyableArticles(String search) {
		List<Article> result = null;
		try {
			Query query = this.em.createQuery("SELECT a FROM " + Article.class.getSimpleName() + " a WHERE a.title LIKE :title AND a.buyer IS NULL", Article.class);
			query.setParameter("title", "%" + search + "%");
			result = query.getResultList();
		} catch (Exception e) {
			return null;
		}
		
		if (!result.isEmpty()) {
			ArrayList<Article> articles = new ArrayList<Article>();
			for (int i = 0; i < result.size(); i ++) {
				long id = result.get(i).getId();
				String title = result.get(i).getTitle();
				String description = result.get(i).getDescription();
				double price = result.get(i).getPrice();
				String currency = result.get(i).getCurrency();
				byte[] image = result.get(i).getImage();
				User user = result.get(i).getSeller();
			
				Article article = new Article();
				article.setId(id);
				article.setTitle(title);
				article.setDescription(description);
				article.setPrice(price);
				article.setCurrency(currency);
				article.setImage(image);
				article.setSeller(user);
			
				articles.add(article);
			}
			return articles;
		} else {
			return null;
		}
	}
}