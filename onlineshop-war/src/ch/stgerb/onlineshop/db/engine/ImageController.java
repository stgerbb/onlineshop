package ch.stgerb.onlineshop.db.engine;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.Part;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
public class ImageController {
	
	public byte[] convertToByteArray(Part foto) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			BufferedImage bufferedFoto = ImageIO.read(foto.getInputStream());
			BufferedImage resized = this.scale(bufferedFoto, 200, 200);
			ImageIO.write(resized, "png", baos);
		} catch (Exception e) {
			return null;
		}
		
		return baos.toByteArray();
	}
	
	private BufferedImage scale(BufferedImage image, int height, int width) {
		Image tmp = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        
        return resized;
	}
}