package ch.stgerb.onlineshop.db.engine;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import ch.stgerb.onlineshop.db.entity.User;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
public class UserEngine {
	
	@PersistenceContext
	private EntityManager em;
	
	@Resource
	private UserTransaction ut;
	
	public boolean saveUser(User user) {
		try {
			this.ut.begin();
			this.em.persist(user);
			this.ut.commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean updateUser(User user) {
		try {
			this.ut.begin();
			this.em.merge(user);
			this.ut.commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean deleteUser(User user) {
		try {
			this.ut.begin();
			User currentUser = this.em.merge(user);
			this.em.remove(currentUser);
			this.ut.commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean isEmailAddressExistent(String emailAddress) {
		try {
			TypedQuery<User> query = this.em.createQuery("SELECT u FROM " + User.class.getSimpleName() + " u WHERE u.emailAddress=:emailAddress", User.class);
			query.setParameter("emailAddress", emailAddress);
			query.getSingleResult();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public User getUserByEmailAddress(String emailAddress) {
		User user = null;
		try {
			TypedQuery<User> query = this.em.createQuery("SELECT u FROM " + User.class.getSimpleName() + " u WHERE u.emailAddress=:emailAddress", User.class);
			query.setParameter("emailAddress", emailAddress);
			user = query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		return user;
	}
	
	public User getUser(long id) {
		User user = this.em.find(User.class, id);
		return user;
	}
}