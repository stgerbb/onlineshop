package ch.stgerb.onlineshop.db.connection.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * @author Stefan Gerber
 * @date 2018-11-22
 */
@WebServlet("/database-connection-test")
public class DatabaseConnectionTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource
	private DataSource dataSource;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=UTF-8");
		PrintWriter printWriter = resp.getWriter();
		printWriter.println("<!DOCTYPE html>");
		printWriter.println("<html>");
		printWriter.println("<head>");
		printWriter.println("</head>");
		printWriter.println("<body>");
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			if(connection.isValid(10)) {
				printWriter.println("<h1>Connected!</h1>");
			} else {
				printWriter.println("<h1>Not connected!</h1>");
			}
		} catch(Exception ex1) {
			printWriter.println("<p>" + ex1.getMessage() + "</p>");
		} finally {
			try {
				connection.close();
			} catch(Exception ex2) {
				printWriter.println("<p>" + ex2.getMessage() + "</p>");
			}
		}
		printWriter.println("<h2>Test finished!</h2>");
		printWriter.println("</body>");
		printWriter.println("</html>");
	}
}