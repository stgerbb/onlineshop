package ch.stgerb.onlineshop.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@Entity
@Table(name="user")
public class User  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private long id;
	
	@Column(name="firstName", nullable=false, length=32)
	private String firstName;
	
	@Column(name="lastName", nullable=false, length=32)
	private String lastName;
	
	@Column(name="emailAddress", nullable=false, length=128)
	private String emailAddress;
	
	@Column(name="password", nullable=false)
	private byte[] password;
	
	@Column(name="salutation", nullable=true, length=8)
	private String salutation;
	
	@Column(name="birthdate", nullable=true, length=16)
	private String birthdate;
	
	@Column(name="street", nullable=true, length=256)
	private String street;
	
	@Column(name="streetNum", nullable=true, length=8)
	private String streetNum;
	
	@Column(name="zip", nullable=true, length=8)
	private String zip;
	
	@Column(name="city", nullable=true, length=64)
	private String city;
	
	@Column(name="country", nullable=true, length=64)
	private String country;
	
	@Column(name="phoneNum", nullable=true, length=32)
	private String phoneNum;
	
	@Lob
	@Column(name="image", nullable=true)
	private byte[] image;
	
	public User() {}
	
	public long getId() { return this.id; }
	
	public String getFirstName() { return this.firstName; }
	
	public String getLastName() { return this.lastName; }
	
	public String getEmailAddress() { return this.emailAddress; }
	
	public byte[] getPassword() { return this.password; }
	
	public String getSalutation() { return this.salutation; }
	
	public String getBirthdate() { return this.birthdate; }
	
	public String getStreet() { return this.street; }
	
	public String getStreetNum() { return this.streetNum; }
	
	public String getZip() { return this.zip; }
	
	public String getCity() { return this.city; }
	
	public String getCountry() { return this.country; }
	
	public String getPhoneNum() { return this.phoneNum; }
	
	public byte[] getImage() { return this.image; }
	
	public void setId(long id) { this.id = id; }
	
	public void setFirstName(String firstName) { this.firstName = firstName; }
	
	public void setLastName(String lastName) { this.lastName = lastName; }
	
	public void setEmailAddress(String emailAddress) { this.emailAddress = emailAddress; }
	
	public void setPassword(byte[] password) { this.password = password; }
	
	public void setSalutation(String salutation) { this.salutation = salutation; }
	
	public void setBirthdate(int day, int month, int year) { this.birthdate = year + "-" + month + "-" + day; }
	
	public void setStreet(String street) { this.street = street; }
	
	public void setStreetNum(String streetNum) { this.streetNum = streetNum; }
	
	public void setZip(String zip) { this.zip = zip; }
	
	public void setCity(String city) { this.city = city; }
	
	public void setCountry(String country) { this.country = country; }
	
	public void setPhoneNum(String phoneNum) { this.phoneNum = phoneNum; }
	
	public void setImage(byte[] image) { this.image = image; }
}