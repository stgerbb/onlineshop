package ch.stgerb.onlineshop.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Stefan Gerber
 * @date 2018-11-15
 */
@Entity
@Table(name="article")
public class Article {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private long id;
	
	@Column(name="title", nullable=false, length=128)
	private String title;
	
	@Column(name="description", nullable=false, length=1024)
	private String description;
	
	@Column(name="price", nullable=false, length=8)
	private double price;
	
	@Column(name="currency", nullable=false, length=8)
	private String currency;
	
	@Lob
	@Column(name="image", nullable=false)
	private byte[] image;
	
	// Hibernate OnDelete
	@ManyToOne
	@JoinColumn(name="seller_id")
	private User seller; 
	
	// Hibernate OnDelete
	@ManyToOne
	@JoinColumn(name="buyer_id")
	private User buyer;
	
	public Article() {}
	
	public long getId() { return this.id; }
	
	public String getTitle() { return this.title; }
	
	public String getDescription() { return this.description; }
	
	public double getPrice() { return this.price; }
	
	public String getCurrency() { return this.currency; }
	
	public byte[] getImage() { return this.image; }
	
	public User getSeller() { return this.seller; }
	
	public User getBuyer() {return this.buyer; }
	
	public void setId(long id) { this.id = id; }
	
	public void setTitle(String title) { this.title = title; }
	
	public void setDescription(String description) { this.description = description; }
	
	public void setPrice(double price) { this.price = price; }
	
	public void setCurrency(String currency) { this.currency = currency; }
	
	public void setImage(byte[] image) { this.image = image; }
	
	public void setSeller(User seller) { this.seller = seller; }
	
	public void setBuyer(User buyer) { this.buyer = buyer; }
 }